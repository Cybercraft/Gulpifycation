'use strict';
// All the libraries needed
var gulp            = require('gulp');
var watch           = require('gulp-watch');
//var imagemin        = require('gulp-imagemin');
var newer           = require('gulp-newer');
var concat          = require('gulp-concat');
var rename          = require('gulp-rename');
//var svgmin          = require('gulp-svgmin');
var sassglob        = require('gulp-sass-glob');
var sass            = require('gulp-sass');
//var cssmin          = require('gulp-minify-css');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('gulp-autoprefixer');
var browserSync     = require('browser-sync');
var runSequence     = require('run-sequence').use(gulp);

var reload          = browserSync.reload;
var debug           = false;

gulp.task('sass', function () {
    debug = true;
    gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sassglob())
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css/'))
        .pipe(reload({stream: true}));
});

// gulp.task('cssmin', function() {
//     return gulp.src('./css/styles.css')
//         .pipe(cssmin())
//         .pipe(rename('styles.min.css'))
//         .pipe(gulp.dest('./css/'))
// });

// add image minify task
// gulp.task('imagemin', function() {
//     return gulp.src(config.paths.imgSrc)
//         .pipe(newer(config.paths.imgSrc))
//         .pipe(imagemin())
//         .pipe(gulp.dest(config.paths.imgDest));
// });

// add svg minify task
// gulp.task('svgmin', function() {
//     return gulp.src(config.paths.svgSrc)
//         .pipe(newer(config.paths.svgSrc))
//         .pipe(svgmin())
//         .pipe(gulp.dest(config.paths.svgDest));
// });

// Run tasks without watching.
gulp.task('build', function(callback) {
    //runSequence('sass', 'imagemin', 'svgmin', 'cssmin', callback);
    runSequence('sass', callback);
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    browserSync({
        proxy: "local.dev"
    });
    gulp.watch('./scss/*.scss', ['sass']);
});

gulp.task('default', function(callback) {
    //runSequence('sass', 'imagemin', 'svgmin', 'cssmin', 'watch', callback);
    runSequence('sass', 'watch', callback);
});
